/* mae.earth/pkg/card/html/html.go
 * mae 012018 */
package html

import (
	"errors"
	"fmt"
	"html/template"
	"strings"

	"mae.earth/pkg/card"
)

var (
	ErrBadData      = errors.New("Bad Data")
	ErrBadCard      = errors.New("Bad Card")
	ErrBadAttribute = errors.New("Bad Attribute")
)

type FormOptions struct {
	Method string
	Action string

	User     FormUser
	Disabled FormDisabled

	Buttons []FormButton
}

type FormAttribute struct {
	Name    string
	Element string
}

type FormButton struct {
	Type  string
	Label string
}

type FormUser map[string]string
type FormDisabled map[string]bool

func (fu FormUser) Get(name, def string) string {
	if v, ok := fu[name]; ok {
		return v
	}
	return def
}

func (fd FormDisabled) Is(name string) string {
	if v, ok := fd[name]; ok {
		if v {
			return "disabled"
		}
	}
	return ""
}

/* GenerateForm */
func GenerateForm(v *card.Card, options *FormOptions) (template.HTML, error) {
	if v == nil || options == nil {
		return template.HTML(""), ErrBadCard
	}

	out := "<form>"

	/* first problem is the order of all the attributes in the card; but we can
	 * sort that by exporting only compounds (which are ordered attributes) */

	data, ok := v.Attributes["data"]
	if !ok {
		return template.HTML(""), ErrBadData
	}

	if string(data.Name()) != card.NameCompound.String() {
		return template.HTML(""), ErrBadData
	}

	comdata := data.(*card.Compound)

	for _, attr := range comdata.Attributes {
		if string(attr.Name()) == card.NameCompound.String() {

			com := attr.(*card.Compound)

			fmt.Printf("\t %d attrs\n", len(com.Attributes))

			if len(com.Attributes) != 3 {
				continue
			}

			var form *FormAttribute
			var err error

			for i, iattr := range com.Attributes {

				switch string(iattr.Name()) {
				case card.NameString.String():

					str := iattr.(card.String)

					fmt.Printf("\t\tstring %s\n", str)

					switch i {
					case 0:

						form, err = ParseFormElement(str.String())
						if err != nil {
							return template.HTML(""), err
						}

						fmt.Printf("\t\t\tparsed as %+v\n", form)

						break
					case 1: /* label string */
						if form == nil {
							return template.HTML(""), ErrBadAttribute
						}

						out += fmt.Sprintf(`<label for="%s">%s</label>`, form.Name, str)

						break
					case 2:

						switch form.Element {
						case "text", "color", "email", "checkbox", "password", "tel", "url":

							out += fmt.Sprintf(`<input id="%s" name="%s" type="%s" value="%s" %s>`,
								form.Name, form.Name, form.Element,
								options.User.Get(form.Name, string(str)), options.Disabled.Is(form.Name))

							break
						}

						break
					}

					break
				case card.NameDelimitedString.String():

					dstr := iattr.(card.DelimitedString)

					fmt.Printf("\t\tdelimited string %s\n", dstr)

					switch i {
					case 0:
						return template.HTML(""), ErrBadAttribute
						break
					case 1:
						if form == nil {
							return template.HTML(""), ErrBadAttribute
						}

						break
					case 2:

						break
					}

					break
				case card.NameCompound.String():

					switch i {
					case 0, 1:
						return template.HTML(""), ErrBadAttribute
						break
					case 2:

						innercom := iattr.(*card.Compound)

						switch form.Element {
						case "select":

							outstr, err := ParseAsSelect(form, innercom, options)
							if err != nil {
								return template.HTML(""), ErrBadAttribute
							}

							out += outstr
							break
						case "radio":

							outstr, err := ParseAsRadio(form, innercom, options)
							if err != nil {
								return template.HTML(""), ErrBadAttribute
							}

							out += outstr
							break
						}

						break
					}

					break
				}

			}

		}
	}

	out += "</form>"

	return template.HTML(out), nil
}

func ParseFormElement(str string) (*FormAttribute, error) {
	/* part-1; part-2
	   * part-1 can = subpart/subpart
		 *
		 * example : form/text; name; foo/bar
	*/

	form := &FormAttribute{}

	for i, part := range strings.Split(str, ";") {

		part = strings.TrimSpace(part)

		switch i {
		case 0:
			subparts := strings.Split(part, "/")
			if subparts[0] != "form" {
				return nil, ErrBadAttribute
			}
			form.Element = subparts[1]
			break
		case 1:
			form.Name = part

			break
		}
	}

	return form, nil
}

func ParseAsSelect(form *FormAttribute, com *card.Compound, options *FormOptions) (string, error) {
	if com == nil || len(com.Attributes) != 2 {
		return "", ErrBadAttribute
	}

	/* first is labels, second is values */
	if string(com.Attributes[0].Name()) != card.NameDelimitedString.String() {
		return "", ErrBadAttribute
	}

	labels := com.Attributes[0].(card.DelimitedString).Delimit()

	if string(com.Attributes[1].Name()) != card.NameDelimitedString.String() {
		return "", ErrBadAttribute
	}

	values := com.Attributes[1].(card.DelimitedString).Delimit()

	if len(labels) != len(values) {
		return "", ErrBadAttribute
	}

	out := fmt.Sprintf(`<select name="%s" %s>`, form.Name, options.Disabled.Is(form.Name))

	def := 0
	for i, value := range values {
		if value[0] == '@' {
			def = i
			values[i] = value[1:]
			break
		}
	}

	defv := options.User.Get(form.Name, values[def])

	for i := 0; i < len(values); i++ {

		label := labels[i]
		value := values[i]

		if value == defv {
			out += fmt.Sprintf(`<option value="%s" selected>%s</option>`, value, label)
		} else {
			out += fmt.Sprintf(`<option value="%s">%s</option>`, value, label)
		}
	}

	out += `</select>`

	return out, nil
}

func ParseAsRadio(form *FormAttribute, com *card.Compound, options *FormOptions) (string, error) {

	if com == nil || len(com.Attributes) != 2 {
		return "", ErrBadAttribute
	}

	/* first is labels, second is values */
	if string(com.Attributes[0].Name()) != card.NameDelimitedString.String() {
		return "", ErrBadAttribute
	}

	labels := com.Attributes[0].(card.DelimitedString).Delimit()

	if string(com.Attributes[1].Name()) != card.NameDelimitedString.String() {
		return "", ErrBadAttribute
	}

	values := com.Attributes[1].(card.DelimitedString).Delimit()

	if len(labels) != len(values) {
		return "", ErrBadAttribute
	}

	out := `<fieldset class="f-radio">`
	//out += fmt.Sprintf(`<legend class="f-radio">%s</legend>`, form.Name)

	def := 0
	for i, value := range values {
		if value[0] == '@' {
			def = i
			values[i] = value[1:]
			break
		}
	}

	defv := options.User.Get(form.Name, values[def])
	disabled := options.Disabled.Is(form.Name)

	for i := 0; i < len(values); i++ {

		label := labels[i]
		value := values[i]

		out += fmt.Sprintf(`<label for="TODO">%s</label>`, label)

		if value == defv {
			out += fmt.Sprintf(`<input class="f-radio" type="radio" name="%s" value="%s" checked %s>`, form.Name, value, disabled)
		} else {
			out += fmt.Sprintf(`<input class="f-radio" type="radio" name="%s" value="%s" %s>`, form.Name, value, disabled)
		}
	}

	out += "</fieldset>"

	return out, nil
}

/*
switch form.Element {
									case "select":

										values := dstr.Delimit()
										def := 0

										for i,value := range values {

											if value[0] == '@' {
												def = i
												values[i] = value[1:]
												break
											}
										}


										out += fmt.Sprintf(`<select name="%s">`, form.Name)

										defv := user.Get(form.Name,values[def])

										for _,value := range values {
											if value == defv {
												out += fmt.Sprintf(`<option value="%s" selected>%s</option>`, value,value)
											} else {
												out += fmt.Sprintf(`<option value="%s">%s</option>`, value,value)
											}
										}

										out += fmt.Sprintf(`</select>`)

						 				break
										case "radio":

											values := dstr.Delimit()
											def := 0
											for i,value := range values {
												if value[0] == '@' {
													def = i
													values[i] = value[1:]
													break
												}
											}

											defv := user.Get(form.Name,values[def])

											for _,value := range values {
												if value == defv {
													out += fmt.Sprintf(`<input type="radio" name="%s" value="%s" checked>`,form.Name,value)
												} else {
													out += fmt.Sprintf(`<input type="radio" name="%s" value="%s">`,form.Name,value)
												}
											}
*/
