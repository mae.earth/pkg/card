/* mae.earth/pkg/card/card_test.go
 * mae 12016 */
package card

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"

	"bytes"
	"compress/flate"
	"compress/lzw"
	"io"

	"mae.earth/pkg/sexpr"
)

var (
	testcard  *Card
	testcard1 *Card
)

func init() {

	c, err := New("")
	if err != nil {
		panic(err)
	}
	c.Set("Alice", String("in wonderland"))
	c.Set("List of Things", DelimitedString{",", "Butterfly,Rabbit,Hamster"})
	c.Set("Id", Byte(101))
	c.Set("Number #", Number(405))
	c.Set("Pi", Real(3.415))
	c.Set("datetime", DatetimeNow())
	com := NewCompound()
	com.Add(String("two"))
	com.Add(DelimitedString{";", "one;two;three;four;five"})
	c.Set("option.1", com)
	com = NewCompound()
	com.Add(Number(2))
	com.Add(DelimitedString{",", "Butterfly,Rabbit,Hamster"})
	c.Set("option.2", com)

	testcard = c

	/* second card with partitions */

	c, err = New("")
	if err != nil {
		panic(err)
	}
	c.Set("public/Alice", String("in wonderland"))
	c.Set("public/List of Things", DelimitedString{",", "Butterfly,Rabbit,Hamster"})
	c.Set("private/Id", Byte(101))
	c.Set("private/Number #", Number(405))
	c.Set("private/Pi", Real(3.415))

	c.Set("datetime", DatetimeNow())

	com = NewCompound()
	com.Add(String("two"))
	com.Add(DelimitedString{";", "one;two;three;four;five"})
	c.Set("public/option.1", com)

	com = NewCompound()
	com.Add(Number(2))
	com.Add(DelimitedString{",", "Butterfly,Rabbit,Hamster"})
	c.Set("private/option.2", com)

	testcard1 = c
}

func Test_Card(t *testing.T) {

	Convey("Version", t, func() {
		root, err := sexpr.ParseString("(version 1)", nil)
		So(err, ShouldBeNil)
		So(root, ShouldNotBeNil)
		//		fmt.Printf("(version 1) = %s\n",cell.Compact(root))
	})

	Convey("Card", t, func() {

		c, err := New("")
		So(err, ShouldBeNil)
		So(c, ShouldNotBeNil)
		So(c.IsData(), ShouldBeTrue)

		c.Version++

		c.Set("Alice", String("in wonderland"))
		c.Set("List of Things", DelimitedString{",", "Butterfly,Rabbit,Hamster"})
		c.Set("info/foo", String("foobar"))
		c.Set("info/bar", String("barfoo"))

		out, err := sexpr.OutputString(c.Sexpr())
		So(err, ShouldBeNil)
		fmt.Printf("%s\n size=%dbytes\n", out, len(out))

		buf := bytes.NewBuffer(nil)
		w := lzw.NewWriter(buf, lzw.LSB, 8)

		io.WriteString(w, out)
		w.Close()

		fmt.Printf("LZW compression size = %dbytes\n", len(buf.Bytes()))

		buf = bytes.NewBuffer(nil)
		w2, err := flate.NewWriter(buf, flate.BestCompression)
		So(err, ShouldBeNil)
		defer w2.Close()

		io.WriteString(w2, out)
		w2.Flush()

		fmt.Printf("DEFLATE compression size = %dbytes\n", len(buf.Bytes()))

		card, err := Parse(out)
		So(err, ShouldBeNil)
		So(card, ShouldNotBeNil)

		//		fmt.Printf("%s\n",card)
		/*
			for key,value := range card.Attributes {
				fmt.Printf("\t%s = %s\n",key,value)
			}
		*/
	})
}

func Test_Complexity(t *testing.T) {
	t.Skip() /* TODO: add created to test */
	test := `(card data 88745ec816d24fc5 5 (A (a "Alice" (string "in wonderland")) (a "List of Things" (delimited-string "," "Butterfly,Rabbit,Hamster")) (a "time" (time "11:39")) (A "foo" (a "bar" (string "foobar")))))`

	Convey("Card Complex", t, func() {
		card, err := Parse(test)
		So(err, ShouldBeNil)
		So(card, ShouldNotBeNil)
		//	fmt.Printf("attributes = %s\n",card.Attributes)

	})
}

func Test_CardParitions(t *testing.T) {

	Convey("Card Partitions", t, func() {
		root, err := Partition(testcard1)
		So(err, ShouldBeNil)
		So(root, ShouldNotBeNil)

		alice := root.GetAttribute("public/Alice")
		So(alice, ShouldNotBeNil)
		So(alice.String(), ShouldEqual, "in wonderland")
	})
}

/* Fuzz crashers */
func Test_FuzzCard(t *testing.T) {

	Convey("Fuzz Crashers", t, func() {
		Convey("c7d88ab75112092c71308d0c8d61551b158811d2", func() {
			card, err := Parse("(card)")
			So(err, ShouldEqual, ErrCardIsInvalid)
			So(card, ShouldBeNil)
		})

		Convey("f9065fa7389750e16fe00d7ba36748f61d3e0df6", func() {
			card, err := Parse("()")
			So(err, ShouldEqual, ErrRootIsInvalid)
			So(card, ShouldBeNil)
		})
	})
}

/* go test -bench=. */

func Benchmark_Card(b *testing.B) {

	test, err := sexpr.OutputString(testcard.Sexpr())
	if err != nil {
		b.Errorf("output error - %v", err)
	}

	for i := 0; i < b.N; i++ {
		card, err := Parse(test)
		if err != nil {
			b.Error(err)
		}
		if card.Identifier.String() != testcard.Identifier.String() {
			b.Errorf("identifier incorrect")
		}
	}
}

/*
func Benchmark_SimpleCards(b *testing.B) {

	test := "(" + NameCard.String() + " " + DataCard.String() + " 88745ec816d24fc5 5 (" + NameAttributes.String() + " (" + NameAttribute.String() + " \"Alice\" (string \"in wonderland\"))"
	test += "(" + NameAttribute.String() + " \"List of Things\" (delimited-string \",\" \"Butterfly,Rabbit,Hamster\"))))"

	for i := 0; i < b.N; i++ {

			card,err := Parse(test)
			if err != nil {
				b.Error(err)
			}
			if card.Identifier.String() != "88745ec816d24fc5" {
				b.Errorf("card identifier incorrect - was %s",card.Identifier.String())
			}

			if card.Version != 5 {
				b.Errorf("card version incorrect - was %d, expecting 5",card.Version)
			}

			if len(card.Attributes) != 2 {
				b.Errorf("card attributes should count 2, but was %d",len(card.Attributes))
			}
  }
}

func Benchmark_ComplexCards(b *testing.B) {

	test := "(" + NameCard.String() + " " + DataCard.String() + " 88745ec816d24fc5 5 (" + NameAttributes.String() + " (" + NameAttribute.String() + " \"Alice\" (string \"in wonderland\"))"
	test += "(" + NameAttribute.String() + " \"List of Things\" (delimited-string \",\" \"Butterfly,Rabbit,Hamster\"))"
	test += "(" + NameAttribute.String() + " \"time\" (time \"11:39\"))(" + NameAttributes.String() + " \"foo\" (" + NameAttribute.String() + " \"bar\" (string \"foobar\")))))"

	for i := 0; i < b.N; i++ {
		card,err := Parse(test)
		if err != nil {
			b.Error(err)
		}

		if _,ok := card.Attributes["foo/bar"]; !ok {
			b.Errorf("foo/bar not found %s",card.Attributes)
		}
	}
}
*/
