module mae.earth/pkg/card

go 1.14

require (
	github.com/smartystreets/goconvey v1.6.4
	mae.earth/pkg/sexpr v0.0.0-20181023150912-cb5ef6629036
)
