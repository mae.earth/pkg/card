/* mae.earth/pkg/card/primitives_test.go
 * mae 12016 */
package card

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func Test_Primitives(t *testing.T) {

	Convey("Parse Primitives from sexpr", t, func() {
		Convey(fmt.Sprintf("Byte (%s)", NameByte), func() {

			s := Byte(0).Sexpr().List()

			b, err := ParseByte(s)
			So(err, ShouldBeNil)
			So(b, ShouldNotBeNil)
			So(byte(b), ShouldEqual, byte(0))

			s = Byte(255).Sexpr().List()

			b, err = ParseByte(s)
			So(err, ShouldBeNil)
			So(b, ShouldNotBeNil)
			So(byte(b), ShouldEqual, byte(255))
		})

		Convey(fmt.Sprintf("Number (%s)", NameNumber), func() {

			s := Number(-1111).Sexpr().List()

			b, err := ParseNumber(s)
			So(err, ShouldBeNil)
			So(b, ShouldNotBeNil)
			So(Number(b), ShouldEqual, Number(-1111))

			s = Number(1010).Sexpr().List()

			b, err = ParseNumber(s)
			So(err, ShouldBeNil)
			So(b, ShouldNotBeNil)
			So(Number(b), ShouldEqual, Number(1010))
		})

		Convey(fmt.Sprintf("String (%s)", NameString), func() {

			s := String("Alice").Sexpr().List()

			b, err := ParseString(s)
			So(err, ShouldBeNil)
			So(b, ShouldNotBeNil)
			So(String(b), ShouldEqual, String("Alice"))

			s = String("").Sexpr().List()

			b, err = ParseString(s)
			So(err, ShouldBeNil)
			So(b, ShouldNotBeNil)
			So(String(b).Equal(String("")), ShouldBeTrue)
		})

		Convey(fmt.Sprintf("Delimited-String (%s)", NameDelimitedString), func() {

			s := DelimitedString{",", "one,two,three,four"}.Sexpr().List()

			b, err := ParseDelimitedString(s)
			So(err, ShouldBeNil)
			So(b, ShouldNotBeNil)
			So(DelimitedString(b).Equal(DelimitedString{",", "one,two,three,four"}), ShouldBeTrue)
		})

		Convey(fmt.Sprintf("Boolean (%s)", NameBoolean), func() {

			s := Boolean(true).Sexpr().List()

			b, err := ParseBoolean(s)
			So(err, ShouldBeNil)
			So(b, ShouldNotBeNil)
			So(Boolean(b).Equal(Boolean(true)), ShouldBeTrue)
			So(Boolean(b).Equal(Boolean(false)), ShouldBeFalse)
		})

		Convey(fmt.Sprintf("Real (%s)", NameReal), func() {

			s := Real(0.001).Sexpr().List()

			b, err := ParseReal(s)
			So(err, ShouldBeNil)
			So(b, ShouldNotBeNil)
			So(Real(b).Equal(Real(0.001)), ShouldBeTrue)
			So(Real(b).Equal(Real(1.000)), ShouldBeFalse)
		})

		Convey(fmt.Sprintf("Datetime (%s)", NameDatetime), func() {

			o := DatetimeNow()
			s := o.Sexpr().List()

			b, err := ParseDatetime(s)
			So(err, ShouldBeNil)
			So(b, ShouldNotBeNil)
			So(Datetime(b).Equal(o), ShouldBeTrue)
		})

	})
}

/* go test -bench=. */

func Benchmark_PrimByte(b *testing.B) {

	test := Byte(101).Sexpr().List()

	for i := 0; i < b.N; i++ {
		a, err := ParseByte(test)
		if err != nil {
			b.Error(err)
		}
		if !Byte(a).Equal(Byte(101)) {
			b.Errorf("byte error")
		}
	}
}

func Benchmark_PrimNumber(b *testing.B) {

	test := Number(101092).Sexpr().List()

	for i := 0; i < b.N; i++ {
		a, err := ParseNumber(test)
		if err != nil {
			b.Error(err)
		}
		if !Number(a).Equal(Number(101092)) {
			b.Errorf("number error")
		}
	}
}

func Benchmark_PrimReal(b *testing.B) {

	test := Real(3.145929).Sexpr().List()

	for i := 0; i < b.N; i++ {
		a, err := ParseReal(test)
		if err != nil {
			b.Error(err)
		}
		if !Real(a).Equal(Real(3.145929)) {
			b.Errorf("error")
		}
	}
}

func Benchmark_PrimString(b *testing.B) {

	test := String("Alice").Sexpr().List()

	for i := 0; i < b.N; i++ {
		a, err := ParseString(test)
		if err != nil {
			b.Error(err)
		}
		if !String(a).Equal(String("Alice")) {
			b.Errorf("error")
		}
	}
}

func Benchmark_PrimDelimitedString(b *testing.B) {

	test := DelimitedString{",", "one,two,three,four,five"}.Sexpr().List()

	for i := 0; i < b.N; i++ {
		a, err := ParseDelimitedString(test)
		if err != nil {
			b.Error(err)
		}
		if !DelimitedString(a).Equal(DelimitedString{",", "one,two,three,four,five"}) {
			b.Errorf("error")
		}
	}
}

func Benchmark_PrimBoolean(b *testing.B) {

	test := Boolean(true).Sexpr().List()

	for i := 0; i < b.N; i++ {
		a, err := ParseBoolean(test)
		if err != nil {
			b.Error(err)
		}
		if !Boolean(a).Equal(Boolean(true)) {
			b.Errorf("error")
		}
	}
}

func Benchmark_PrimDatetime(b *testing.B) {

	org := DatetimeNow()
	test := org.Sexpr().List()

	for i := 0; i < b.N; i++ {
		a, err := ParseDatetime(test)
		if err != nil {
			b.Error(err)
		}
		if !Datetime(a).Equal(org) {
			b.Errorf("error")
		}
	}
}
