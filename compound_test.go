/* mae.earth/pkg/card/compound_test.go
 * mae 12016 */
package card

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func Test_Compound(t *testing.T) {

	Convey(fmt.Sprintf("Compound (%s)", NameCompound), t, func() {
		Convey("Primitives", func() {
			com := NewCompound()
			com.Label = "test"
			So(com, ShouldNotBeNil)

			com.Add(Byte(0))
			com.Add(Number(101))
			com.Add(String("Alice"))
			com.Add(DelimitedString{",", "one,two,three"})
			com.Add(Boolean(false))
			So(len(com.Attributes), ShouldEqual, 5)

			//		out,_ := sexpr.OutputString(com.Sexpr())
			//	fmt.Printf("sexpr = %s\n",out)
		})

		Convey(fmt.Sprintf("Parse (%s) should be empty error", NameCompound), func() {
			com := NewCompound()
			So(com, ShouldNotBeNil)

			com1, err := ParseCompound(com.Sexpr().List())
			So(err, ShouldEqual, ErrRootIsNil)
			So(com1, ShouldBeNil)
		})

		Convey(fmt.Sprintf("Parse (%s %s %s)", NameCompound, NameString, NameDelimitedString), func() {
			com := NewCompound()
			com.Label = "test"
			So(com, ShouldNotBeNil)

			com.Add(String("two"))
			com.Add(DelimitedString{",", "one,two,three,four,five"})

			com1, err := ParseCompound(com.Sexpr().List())
			So(err, ShouldBeNil)
			So(com1, ShouldNotBeNil)
			So(com.Equal(com1), ShouldBeTrue)
		})

		Convey(fmt.Sprintf("Parse (%s (%s %s %s)) should be in error", NameCompound, NameCompound, NameString, NameDelimitedString), func() {
			com := NewCompound()
			So(com, ShouldNotBeNil)

			com1 := NewCompound()
			So(com1, ShouldNotBeNil)

			com1.Add(String("two"))
			com1.Add(DelimitedString{",", "one,two,three,four,five"})
			So(len(com1.Attributes), ShouldEqual, 2)

			com.Add(com1)
			So(len(com.Attributes), ShouldEqual, 1)

			r, err := ParseCompound(com.Sexpr().List())
			So(err, ShouldEqual, ErrInvalidValue)
			So(r, ShouldBeNil)
		})

	})
}

/* go test -bench=. */

func Benchmark_CompoundA(b *testing.B) {

	com := NewCompound()
	com.Add(String("one"))
	com.Add(DelimitedString{",", "one,two,three,four"})

	test := com.Sexpr().List()

	for i := 0; i < b.N; i++ {
		_, err := ParseCompound(test)
		if err != nil {
			b.Error(err)
		}
	}
}

func Benchmark_CompoundB(b *testing.B) {

	com := NewCompound()
	com.Add(Number(1))
	com.Add(DelimitedString{",", "one,two,three,four"})

	test := com.Sexpr().List()

	for i := 0; i < b.N; i++ {
		_, err := ParseCompound(test)
		if err != nil {
			b.Error(err)
		}
	}
}

func Benchmark_CompoundC(b *testing.B) {

	com := NewCompound()
	com.Add(Byte(1))
	com.Add(DelimitedString{",", "one,two,three,four"})

	test := com.Sexpr().List()

	for i := 0; i < b.N; i++ {
		_, err := ParseCompound(test)
		if err != nil {
			b.Error(err)
		}
	}
}

func Benchmark_CompoundD(b *testing.B) {
	b.Skip() /* FIXME, this seems broken */

	com := NewCompound()
	com.Add(DatetimeNow())
	com.Add(DelimitedString{",", "one,two,three,four"})

	test := com.Sexpr().List()

	for i := 0; i < b.N; i++ {
		_, err := ParseCompound(test)
		if err != nil {
			b.Error(err)
		}
	}
}
