/* mae.earth/pkg/card/compound.go
 * mae 12016 */
package card

import (
	"mae.earth/pkg/sexpr/cell"
)

type Compound struct {
	Label      string
	Attributes []Attributer
}

func (c Compound) Equal(other Attributer) bool {
	if a, ok := other.(Compound); ok {
		if a.Label != c.Label {
			return false
		}

		if len(c.Attributes) != len(a.Attributes) {
			return false
		}
		for i, val := range a.Attributes {
			if !c.Attributes[i].Equal(val) {
				return false
			}
		}
	}
	return true
}

func (c Compound) Sexpr() *cell.Cell {

	inner := cell.New(NameCompound, nil)
	if len(c.Label) > 0 {
		inner = cell.Append(inner, cell.New(Literal(c.Label), nil))
	}

	for _, val := range c.Attributes {
		inner = cell.Append(inner, val.Sexpr())
	}
	return cell.Open(inner)
}

func (c Compound) String() string {
	str := "(" + c.Label + ") "
	for _, val := range c.Attributes {
		if len(str) > 0 {
			str += "+"
		}
		str += val.String()
	}
	return str
}

func (c Compound) Name() Name {
	return NameCompound
}

func (c Compound) Copy() Attributer {

	c0 := NewCompound()
	c0.Label = c.Label
	for _, attr := range c.Attributes {
		c0.Attributes = append(c0.Attributes, attr.Copy())
	}
	return c0
}

func (c Compound) Update(interface{}) Attributer {
	return c
}

func (c Compound) Blank() Attributer {
	return c
}

func (c *Compound) Add(val Attributer) *Compound {
	c.Attributes = append(c.Attributes, val)
	return c
}

func NewCompound() *Compound {

	c := &Compound{}
	c.Attributes = make([]Attributer, 0)
	return c
}

func ParseCompound(root *cell.Cell) (*Compound, error) {
	root = root.Next()
	if root == nil {
		return nil, ErrRootIsNil
	}
	com := NewCompound()
	for {
		if root == nil {
			break
		}

		if root.IsList() {
			r := root.List()
			val, ok := r.ToSValue()
			if !ok {
				return nil, ErrInvalidValue
			}

			switch val { /* just primitives */
			case NameString.String():
				v, err := ParseString(r)
				if err != nil {
					return nil, err
				}
				com.Add(v)
				break
			case NameDelimitedString.String():
				v, err := ParseDelimitedString(r)
				if err != nil {
					return nil, err
				}
				com.Add(v)
				break
			case NameBoolean.String():
				v, err := ParseBoolean(r)
				if err != nil {
					return nil, err
				}
				com.Add(v)
				break
			case NameByte.String():
				v, err := ParseByte(r)
				if err != nil {
					return nil, err
				}
				com.Add(v)
				break
			case NameNumber.String():
				v, err := ParseNumber(r)
				if err != nil {
					return nil, err
				}
				com.Add(v)
				break
			case NameReal.String():
				v, err := DefaultReal.Parse(r)
				if err != nil {
					return nil, err
				}
				com.Add(v)
				break
			default:
				return nil, ErrInvalidValue
				break
			}

		} else {
			com.Label = root.Value().String()
		}

		root = root.Next()
	}
	return com, nil
}
