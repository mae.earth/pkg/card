/* mae.earth/pkg/card/card.go
 * mae 12016 */
package card

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"hash/crc32"
	"io"
	"strconv"
	"strings"
	"sync"
	"time"

	"mae.earth/pkg/sexpr"
	"mae.earth/pkg/sexpr/cell"
)

type UserParser interface {
	Parse(root *cell.Cell) (Attributer, error)
}

var internal struct {
	sync.RWMutex

	parsers map[string]UserParser
}

func init() {
	internal.parsers = make(map[string]UserParser, 0)

}

/* RegisterUserParser */
func RegisterUserParser(name string, p UserParser) bool {
	internal.Lock()
	defer internal.Unlock()

	internal.parsers[name] = p
	return true
}

/* DeregisterUserParser */
func DeregisterUserParser(name string) bool {
	internal.Lock()
	defer internal.Unlock()

	if _, ok := internal.parsers[name]; ok {
		delete(internal.parsers, name)
		return true
	}
	return false
}

const (
	UPID = 4 /* Unique Project IDentifier */
	PID  = 4 /* Process IDentifier */
)

type Sumer interface {
	Sum() uint
}

type Identifier string

func (id Identifier) String() string {
	return string(id)
}

func (id Identifier) Sum() uint {
	return uint(crc32.ChecksumIEEE([]byte(id)))
}

func (id Identifier) IsInvalid() bool {
	return (len(id) == 0)
}

/* Generate */
func Generate(size uint) (Identifier, error) {

	b := make([]byte, size)
	_, err := io.ReadFull(rand.Reader, b)
	if err != nil {
		return Identifier(""), err
	}
	return Identifier(hex.EncodeToString(b)), nil
}

const PartDelimiter string = "@"

func Compact(root *cell.Cell) string {
	return cell.Compact(root)
}

/* TODO: convert io interfaces to io.writer */

const (
	NameCard       = Name("card")
	NameAttributes = Name("A") //Name("attributes")
	NameAttribute  = Name("a") //Name("attribute")
	NameCompound   = Name("C")
)

const (
	DataCard   = Type("data")
	FilterCard = Type("filter")
	ViewCard   = Type("view")
)

var (
	ErrRootIsNil     = fmt.Errorf("Root is nil")
	ErrRootIsInvalid = fmt.Errorf("Root is Invalid")
	ErrInvalidValue  = fmt.Errorf("Value is invalid")
	ErrCardIsInvalid = fmt.Errorf("Card is invalid")
)

type Literal string

func (l Literal) String() string {
	return fmt.Sprintf("\"%s\"", string(l))
}

type Plain string

func (s Plain) String() string {
	return string(s)
}

type Name string

func (n Name) String() string {
	return string(n)
}

type Type string

func (t Type) String() string {
	return string(t)
}

type Attributer interface {
	Sexpr() *cell.Cell
	String() string
	Name() Name
	Copy() Attributer
	Update(interface{}) Attributer
	Blank() Attributer
	Equal(Attributer) bool
}

func S(h cell.Stringer) *cell.Cell {
	return cell.New(h, nil)
}

func Strip(s string) string {
	if s[0] == '"' {
		return s[1 : len(s)-1]
	}
	return s
}

type Card struct {
	Identifier
	Type
	Version int
	Created time.Time

	Attributes map[string]Attributer
}

func (c *Card) Names() []string {
	if len(c.Attributes) == 0 {
		return nil
	}
	names := make([]string, 0)
	for key := range c.Attributes {
		names = append(names, key)
	}
	return names
}

func (c *Card) String() string {
	return fmt.Sprintf("card type=%s id=%s version=%d, %d attribute(s)", c.Type, c.Identifier, c.Version, len(c.Attributes))
}

func (c *Card) Sexpr() *cell.Cell {

	root := cell.New(NameCard, nil)
	root = cell.Append(root, S(Plain(c.Type.String())))
	root = cell.Append(root, S(Plain(c.Identifier.String())))
	root = cell.Append(root, S(Plain(strconv.Itoa(c.Version))))
	root = cell.Append(root, S(Literal(c.Created.Format(time.RubyDate))))

	attrs := cell.New(NameAttributes, nil)
	/* FIXME : cut all the key names into trees */

	for key, val := range c.Attributes {
		at := cell.New(NameAttribute, nil)
		at = cell.Append(at, cell.New(Literal(key), nil))
		at = cell.Append(at, val.Sexpr())
		attrs = cell.Append(attrs, cell.Open(at))
	}

	return cell.Open(cell.Append(root, S(attrs)))
}

func (c *Card) Set(key string, val Attributer) *Card {
	if val == nil {
		delete(c.Attributes, key)
		return c
	}

	c.Attributes[key] = val
	return c
}

func (c *Card) Get(key string) Attributer {

	val, ok := c.Attributes[key]
	if !ok {
		return nil
	}
	return val
}

func (c *Card) IsData() bool {
	return (c.Type == DataCard)
}

func (c *Card) SetAsFilter() *Card {
	c.Type = FilterCard
	return c
}

func (c *Card) SetAsView() *Card {
	c.Type = ViewCard
	return c
}

func (c *Card) SetAsData() *Card {
	c.Type = DataCard
	return c
}

func New(id Identifier) (*Card, error) {
	var err error
	if id.IsInvalid() {
		id, err = Generate(UPID)
		if err != nil {
			return nil, err
		}
	}

	c := &Card{}
	c.Version = 1
	c.Type = DataCard /* default */
	c.Identifier = id
	c.Created = time.Now()
	c.Attributes = make(map[string]Attributer, 0)
	return c, nil
}

func Clone(c *Card) (*Card, error) {
	if c == nil {
		return nil, ErrRootIsNil
	}
	c1, err := New("")
	if err != nil {
		return nil, err
	}

	c1.Type = c.Type
	for key, attr := range c.Attributes {
		if attr.Name() == NameCompound {
			if com, ok := attr.(*Compound); ok {
				c1.Attributes[key] = com.Copy()
			}
		} else {
			c1.Attributes[key] = attr.Copy()
		}
	}
	return c1, nil
}

func CloneBlank(c *Card) (*Card, error) {
	if c == nil {
		return nil, ErrRootIsNil
	}
	c1, err := New("")
	if err != nil {
		return nil, err
	}

	c1.Type = c.Type
	for key, attr := range c.Attributes {
		if attr.Name() == NameCompound {
			if com, ok := attr.(*Compound); ok {
				c1.Attributes[key] = com.Copy()
			}
		} else {
			c1.Attributes[key] = attr.Copy().Blank()
		}
	}
	return c1, nil
}

func attribute(card *Card, p string, r *cell.Cell) error {
	var name string

	if r == nil {
		return ErrRootIsNil
	}
	v, ok := r.ToSValue()
	if !ok {
		return ErrInvalidValue
	}
	name = Strip(v)
	if len(p) > 0 {
		name = p + "/" + name
	}
	r = r.Next()
	if !r.IsList() {
		return ErrInvalidValue
	}

	r = r.List()
	val, ok := r.ToSValue()
	if !ok {
		return ErrInvalidValue
	}

	switch val { /* primitives + compounds */
	case NameString.String():
		v, err := ParseString(r)
		if err != nil {
			return err
		}

		card.Set(name, v)
		break
	case NameDelimitedString.String():
		v, err := ParseDelimitedString(r)
		if err != nil {
			return err
		}
		card.Set(name, v)
		break
	case NameBoolean.String():
		v, err := ParseBoolean(r)
		if err != nil {
			return err
		}
		card.Set(name, v)
		break
	case NameByte.String():
		v, err := ParseByte(r)
		if err != nil {
			return err
		}
		card.Set(name, v)
		break
	case NameNumber.String():
		v, err := ParseNumber(r)
		if err != nil {
			return err
		}
		card.Set(name, v)
		break
	case NameReal.String():
		v, err := DefaultReal.Parse(r)
		if err != nil {
			return err
		}
		card.Set(name, v)
		break
	/* compounds */
	case NameCompound.String():
		v, err := ParseCompound(r)
		if err != nil {
			return err
		}
		card.Set(name, v)
		break
	default:
		internal.RLock()

		if p, ok := internal.parsers[val]; ok {
			v, err := p.Parse(r)
			if err != nil {
				internal.RUnlock()
				return err
			}
			card.Set(name, v)
		}

		internal.RUnlock()
		break
	}
	return nil
}

func attributes(card *Card, p string, r *cell.Cell) error {
	if r == nil {
		return ErrRootIsNil
	}

	if r.IsValue() {
		val, ok := r.ToSValue()
		if !ok {
			return ErrInvalidValue
		}
		if len(p) > 0 {
			p += PartDelimiter + Strip(val)
		} else {
			p = Strip(val)
		}

		r = r.Next()
	}

	for {
		if r == nil {
			break
		}

		if r.IsList() {

			c := r.List()
			val, ok := c.ToSValue()
			if !ok {
				return ErrInvalidValue
			}

			if val == NameAttributes.String() {
				c = c.Next()
				if c.IsValue() {
					val, ok = c.ToSValue()
					if !ok {
						return ErrInvalidValue
					}
					np := Strip(val)
					if len(p) > 0 && len(val) > 2 {
						np = p + PartDelimiter + Strip(val)
					}

					if err := attributes(card, np, c.Next()); err != nil {
						return err
					}
				}
			} else if val == NameAttribute.String() {

				if err := attribute(card, p, c.Next()); err != nil {
					return err
				}
			}
		}

		r = r.Next()
	}
	return nil
}

func Parse(f string) (*Card, error) { /* FIXME, should be a io.reader -> through a compression layer */

	root, err := sexpr.ParseString(f, nil)
	if err != nil {
		return nil, err
	}

	if root == nil {
		return nil, ErrRootIsNil
	}

	if !root.IsList() {
		return nil, ErrRootIsInvalid
	}

	c := root.List()

	if c == nil {
		return nil, ErrRootIsNil
	}

	if val, ok := c.ToSValue(); ok {
		if val != NameCard.String() {
			return nil, fmt.Errorf("Not a valid card")
		}
	} else {
		return nil, ErrRootIsInvalid
	}

	c = c.Next()
	t := DataCard

	if c == nil {
		return nil, ErrCardIsInvalid
	}

	if val, ok := c.ToSValue(); ok {
		t = Type(val)
		/* TODO: check */
	} else {
		return nil, fmt.Errorf("Card Type is not a value")
	}

	c = c.Next()

	if c == nil {
		return nil, ErrCardIsInvalid
	}

	val, ok := c.ToSValue()
	if !ok {
		return nil, fmt.Errorf("identity is not a value")
	}
	card, err := New(Identifier(val))
	if err != nil {
		return nil, err
	}
	card.Type = t

	c = c.Next()

	if c == nil {
		return nil, ErrCardIsInvalid
	}

	val, ok = c.ToSValue()
	if !ok {
		return nil, fmt.Errorf("version is not a value")
	}

	ver, err := strconv.Atoi(val)
	if err != nil {
		return nil, err
	}
	card.Version = ver

	c = c.Next()

	if c == nil {
		return nil, ErrCardIsInvalid
	}

	val, ok = c.ToSValue()
	if !ok {
		return nil, fmt.Errorf("expected created value")
	}

	ti, err := time.Parse(time.RubyDate, Strip(val))
	if err != nil {
		return nil, err
	}

	card.Created = ti

	c = c.Next()
	if c == nil {
		return nil, ErrCardIsInvalid
	}

	if !c.IsList() {
		return nil, fmt.Errorf("expecting attributes")
	}

	c = c.List()
	if c == nil {
		return nil, ErrCardIsInvalid
	}

	val, ok = c.ToSValue()
	if !ok {
		return nil, fmt.Errorf("expecting attributes")
	}
	if val != NameAttributes.String() {
		return nil, fmt.Errorf("expecting attributes got %s", val)
	}

	c = c.Next()

	for {
		if c == nil {
			break
		}

		if c.IsList() {
			b := c.List()
			val, ok := b.ToSValue()
			if !ok {
				return nil, ErrInvalidValue
			}

			if val == NameAttribute.String() {

				if err := attribute(card, "", b.Next()); err != nil {
					return nil, err
				}
			} else if val == NameAttributes.String() {

				if err := attributes(card, "", b.Next()); err != nil {
					return nil, err
				}

			}
		}

		c = c.Next()
	}

	return card, nil
}

func Output(card *Card) (string, error) {
	return sexpr.OutputString(card.Sexpr())
}

type Part struct {
	Name       string
	Attributes map[string]Attributer
	Children   map[string]*Part
}

func (p *Part) String() string {
	return fmt.Sprintf("part [%s] %d attributes, %d children", p.Name, len(p.Attributes), len(p.Children))
}

func (p *Part) AddAttribute(key string, attr Attributer) *Part {

	parts := strings.Split(key, PartDelimiter)

	if len(parts) == 1 {
		p.Attributes[parts[0]] = attr
	} else {
		var pc *Part
		pc = p
		for i, part := range parts {
			if (i + 1) == len(parts) {
				pc.Attributes[part] = attr
				break
			}

			if _, ok := pc.Children[part]; !ok {
				pc.Children[part] = NewPart(part)
			}

			pc = pc.Children[part]

		}
	}

	return p
}

func (p *Part) GetAttribute(key string) Attributer {
	parts := strings.Split(key, PartDelimiter)

	if len(parts) == 1 {
		if val, ok := p.Attributes[parts[0]]; ok {
			return val
		}
	} else {
		var pc *Part
		var ok bool
		pc = p

		for i, part := range parts {
			if (i + 1) == len(parts) {
				if attr, ok := pc.Attributes[part]; ok {
					return attr
				}
				break
			}
			pc, ok = pc.Children[part]
			if !ok {
				return nil
			}
		}
	}
	return nil
}

func NewPart(name string) *Part {
	p := &Part{Name: name}
	p.Attributes = make(map[string]Attributer, 0)
	p.Children = make(map[string]*Part, 0)
	return p
}

/* Go through the attributes of a card and partition into logical parts based on
 * the attributes name - pretty much like a file in a path
 */
func Partition(card *Card) (*Part, error) {

	root := NewPart("")

	for key, attr := range card.Attributes {
		root.AddAttribute(key, attr)
	}

	return root, nil
}

/* see https://github.com/dvyukov/go-fuzz */
func Fuzz(data []byte) int {
	if _, err := Parse(string(data)); err != nil {
		return 0
	}
	return 1
}
