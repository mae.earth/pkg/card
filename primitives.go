/* mae.earth/pkg/card/primitives.go
 * mae 12016 */
package card

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"mae.earth/pkg/sexpr/cell"
)

const (
	NameByte            = Name("uint8")
	NameNumber          = Name("int")
	NameString          = Name("string")
	NameDelimitedString = Name("delimited-string")
	NameBoolean         = Name("boolean")
	NameReal            = Name("real")
	NameDatetime        = Name("datetime")
)

var (
	EmptyDatetime = Datetime(time.Now())
)

/* TODO: add example outputs in sexpr form */

/* Datetime */
type Datetime time.Time

func DatetimeNow() Datetime {
	snow := time.Now().Format(time.RubyDate)
	now, _ := time.Parse(time.RubyDate, snow)
	return Datetime(now)
}

func (dt Datetime) Equal(other Attributer) bool {
	if a, ok := other.(Datetime); ok {
		return time.Time(a).Equal(time.Time(dt))
	}
	return false
}

func (dt Datetime) Blank() Attributer {
	return Datetime(time.Now().UTC())
}

func (dt Datetime) Update(u interface{}) Attributer {
	if au, ok := u.(time.Time); ok {
		return Datetime(au)
	}
	return dt
}

func (dt Datetime) Name() Name {
	return NameDatetime
}

func (dt Datetime) String() string {
	return time.Time(dt).Format(time.RubyDate) //RFC3339)
}

func (dt Datetime) Sexpr() *cell.Cell {
	return cell.Open(cell.List(NameDatetime, Literal(dt.String())))
}

func (dt Datetime) Copy() Attributer {
	return Datetime(dt)
}

func ParseDatetime(root *cell.Cell) (Datetime, error) {
	root = root.Next()
	if root == nil {
		return EmptyDatetime, ErrRootIsNil
	}
	val, ok := root.ToSValue()
	if !ok {
		return EmptyDatetime, ErrInvalidValue
	}
	t, err := time.Parse(time.RubyDate, Strip(val))
	if err != nil {
		return EmptyDatetime, err
	}
	return Datetime(t), nil
}

/* real */
type Real float64

const DefaultReal = Real(0.0)

func (r Real) Equal(other Attributer) bool {
	if a, ok := other.(Real); ok {
		return (a == r)
	}
	return false
}

func (r Real) Blank() Attributer {
	return Real(0.0)
}

func (r Real) Update(u interface{}) Attributer {
	if au, ok := u.(float64); ok {
		return Real(au)
	}
	return r
}

func (r Real) Name() Name {
	return NameReal
}

func (r Real) String() string {
	return f64(float64(r))
}

func (r Real) Sexpr() *cell.Cell {
	return cell.Open(cell.List(NameReal, Plain(f64(float64(r)))))
}

func (r Real) Copy() Attributer {
	return Real(r)
}

func (r Real) Parse(root *cell.Cell) (Real, error) {
	return ParseReal(root)
}

func ParseReal(root *cell.Cell) (Real, error) {
	root = root.Next()
	if root == nil {
		return Real(0.0), ErrRootIsNil
	}
	val, ok := root.ToSValue()
	if !ok {
		return Real(0.0), ErrInvalidValue
	}
	f, err := strconv.ParseFloat(val, 64)
	if err != nil {
		return Real(0.0), err
	}
	return Real(f), nil
}

/* number */
type Number int

func (n Number) Equal(other Attributer) bool {
	if a, ok := other.(Number); ok {
		return (a == n)
	}
	return false
}

func (n Number) Blank() Attributer {
	return Number(0)
}

func (n Number) Update(u interface{}) Attributer {
	if au, ok := u.(int); ok {
		return Number(au)
	}
	return n
}

func (n Number) Name() Name {
	return NameNumber
}

func (n Number) String() string {
	return fmt.Sprintf("%d", n)
}

func (n Number) Sexpr() *cell.Cell {
	return cell.Open(cell.List(NameNumber, Plain(fmt.Sprintf("%d", n))))
}

func (n Number) Copy() Attributer {
	return Number(n)
}

func ParseNumber(root *cell.Cell) (Number, error) {
	root = root.Next()
	if root == nil {
		return Number(0), ErrRootIsNil
	}
	val, ok := root.ToSValue()
	if !ok {
		return Number(0), ErrInvalidValue
	}
	i, err := strconv.Atoi(val)
	if err != nil {
		return Number(0), err
	}
	return Number(i), nil
}

/* byte */
type Byte byte

func (b Byte) Equal(other Attributer) bool {
	if a, ok := other.(Byte); ok {
		return (a == b)
	}
	return false
}

func (b Byte) Blank() Attributer {
	return Byte(0)
}

func (b Byte) Update(u interface{}) Attributer {
	if au, ok := u.(byte); ok {
		return Byte(au)
	}
	return b
}

func (b Byte) Name() Name {
	return NameByte
}

func (b Byte) String() string {
	return fmt.Sprintf("%03d", b)
}

func (b Byte) Sexpr() *cell.Cell {
	return cell.Open(cell.List(NameByte, Plain(fmt.Sprintf("%03d", b))))
}

func (b Byte) Copy() Attributer {
	return Byte(b)
}

func ParseByte(root *cell.Cell) (Byte, error) {
	root = root.Next()
	if root == nil {
		return Byte(0), ErrRootIsNil
	}
	val, ok := root.ToSValue()
	if !ok {
		return Byte(0), ErrInvalidValue
	}
	i, err := strconv.Atoi(val)
	if err != nil {
		return Byte(0), err
	}
	if i < 0 {
		i = 0
	}
	if i > 255 {
		i = 255
	}
	return Byte(byte(i)), nil
}

/* String */
type String string

func (s String) Equal(other Attributer) bool {
	if a, ok := other.(String); ok {
		return (a == s)
	}
	return false
}

func (s String) Blank() Attributer {
	return String("")
}

func (s String) Update(u interface{}) Attributer {
	if au, ok := u.(string); ok {
		return String(au)
	}
	return s
}

func (s String) Name() Name {
	return NameString
}

func (s String) String() string {
	return string(s)
}

func (s String) Sexpr() *cell.Cell {
	return cell.Open(cell.List(NameString, Literal(string(s))))
}

func (s String) Copy() Attributer {
	return String(s)
}

func ParseString(root *cell.Cell) (String, error) {
	root = root.Next()
	if root == nil {
		return "", ErrRootIsNil
	}
	val, ok := root.ToSValue()
	if !ok {
		return "", ErrInvalidValue
	}

	return String(Strip(val)), nil
}

/* Boolean */
type Boolean bool

func (b Boolean) Equal(other Attributer) bool {
	if a, ok := other.(Boolean); ok {
		return (a == b)
	}
	return false
}

func (b Boolean) Blank() Attributer {
	return Boolean(false)
}

func (b Boolean) Update(u interface{}) Attributer {
	if au, ok := u.(bool); ok {
		return Boolean(au)
	}
	return b
}

func (b Boolean) Name() Name {
	return NameBoolean
}

func (b Boolean) String() string {
	o := "no"
	if bool(b) == true {
		o = "yes"
	}
	return o
}

func (b Boolean) Sexpr() *cell.Cell {
	v := 0
	if bool(b) {
		v = 1
	}
	return cell.Open(cell.List(NameBoolean, Plain(fmt.Sprintf("%d", v))))
}

func (b Boolean) Copy() Attributer {
	return Boolean(bool(b))
}

func ParseBoolean(root *cell.Cell) (Boolean, error) {
	root = root.Next()
	if root == nil {
		return false, ErrRootIsNil
	}
	val, ok := root.ToSValue()
	if !ok {
		return false, ErrInvalidValue
	}
	b, err := strconv.Atoi(val)
	if err != nil {
		return false, err
	}
	return Boolean((b != 0)), nil
}

type DelimitedString struct {
	Delimiter string /* default ',' */
	Content   string
}

func (s DelimitedString) Delimit() []string {
	return strings.Split(s.Content, s.Delimiter)
}

func (s DelimitedString) Equal(other Attributer) bool {
	if a, ok := other.(DelimitedString); ok {
		if a.Delimiter != s.Delimiter {
			return false
		}
		return (a.Content == s.Content)
	}
	return false
}

func (s DelimitedString) Blank() Attributer {
	return DelimitedString{s.Delimiter, ""}
}

func (s DelimitedString) Update(u interface{}) Attributer {
	if au, ok := u.(string); ok {
		return DelimitedString{s.Delimiter, au}
	}
	return s
}

func (s DelimitedString) Name() Name {
	return NameDelimitedString
}

func (s DelimitedString) String() string {
	return s.Content
}

func (s DelimitedString) Copy() Attributer {
	return DelimitedString{s.Delimiter, s.Content}
}

func ParseDelimitedString(root *cell.Cell) (DelimitedString, error) {
	root = root.Next()
	if root == nil {
		return DelimitedString{}, ErrRootIsNil
	}
	del := ","
	v, ok := root.ToSValue()
	if !ok {
		return DelimitedString{}, ErrInvalidValue
	}
	del = Strip(v)

	root = root.Next()
	if root == nil {
		return DelimitedString{}, ErrRootIsNil
	}
	v, ok = root.ToSValue()
	if !ok {
		return DelimitedString{}, ErrInvalidValue
	}
	return DelimitedString{del, Strip(v)}, nil
}

func (ds DelimitedString) Sexpr() *cell.Cell {
	return cell.Open(cell.List(NameDelimitedString, Literal(ds.Delimiter), Literal(ds.Content)))
}

func f64(f float64) string {

	str := fmt.Sprintf("%f", f)
	s := 0
	neg := false
	for i, c := range str {
		if c != '0' {
			if c == '-' {
				neg = true
				continue
			}
			s = i
			break
		}
		if c == '.' {
			break
		}
	}

	e := 0
	for i := len(str) - 1; i >= 0; i-- {
		if str[i] != '0' {
			e = i + 1
			break
		}
		if str[i] == '.' {
			break
		}
	}

	str = str[s:e]
	if str == "." {
		str = "0"
	}
	if neg {
		str = "-" + str
	}
	if str[len(str)-1] == '.' {
		str = str[:len(str)-1]
	}

	return str
}
