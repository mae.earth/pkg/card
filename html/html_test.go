/* mae.earth/pkg/card/html/html_test.go
 * mae 012018 */
package html

import (
	"fmt"
	"io/ioutil"
	"mae.earth/pkg/card"
	"strings"

	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func input(element, name, label, value string) *card.Compound {

	com := card.NewCompound()
	com.Add(card.String(fmt.Sprintf("form/%s; %s", element, name)))
	com.Add(card.String(label))

	switch element {
	case "text", "color", "checkbox", "email", "password", "tel", "url":
		com.Add(card.String(value))
		break
	case "select", "radio":
		com.Add(card.DelimitedString{";", value})
		break
	}

	return com
}

func listv(items ...string) []string {
	return items
}

func inputradio(name, grouplabel string, labels, values []string) *card.Compound {

	com := card.NewCompound()
	com.Add(card.String(fmt.Sprintf("form/radio; %s", name)))
	com.Add(card.String(grouplabel))

	inner := card.NewCompound()
	inner.Add(card.DelimitedString{";", strings.Join(labels, ";")})
	inner.Add(card.DelimitedString{";", strings.Join(values, ";")})

	com.Add(inner)

	return com
}

func inputselect(name, grouplabel string, labels, values []string) *card.Compound {

	com := card.NewCompound()
	com.Add(card.String(fmt.Sprintf("form/select; %s", name)))
	com.Add(card.String(grouplabel))

	inner := card.NewCompound()
	inner.Add(card.DelimitedString{";", strings.Join(labels, ";")})
	inner.Add(card.DelimitedString{";", strings.Join(values, ";")})

	com.Add(inner)

	return com
}

func Test_HTML(t *testing.T) {
	Convey("HTML", t, func() {

		/* example take a card with values and generate html form */

		c, err := card.New("")
		So(err, ShouldBeNil)

		/* GenerateForm only exports ordered attributes, so we need to use a
		 * compound to arrange that order
		 */

		list := card.NewCompound()

		list.Add(input("text", "text-1", "this is a text", "alice was here"))
		list.Add(input("checkbox", "checkbox-1", "this is a checkbox", "fred"))
		list.Add(input("color", "color-1", "this is a colorbox", "purple"))
		list.Add(input("email", "email-1", "this is an email", "alice@wonder.land"))
		list.Add(input("hidden", "hidden-1", "this is hidden", "hidden"))
		list.Add(input("password", "password-1", "this is a password", "abc"))
		list.Add(inputradio("radio-1", "this is a radio", listv("One", "Two", "Three", "Four", "Five", "Six"), listv("one", "two", "three", "@four", "five", "six")))
		list.Add(input("tel", "tel-1", "this is a tel", "0123456789"))
		list.Add(input("url", "url-1", "this is an url", "http://example.org/url"))
		list.Add(inputselect("select-1", "this is a select", listv("One", "Two", "Three", "Four", "Five", "Six"), listv("one", "two", "@three", "four", "five", "six")))

		c.Set("data", list)

		user := make(map[string]string, 0)
		user["text-1"] = "Alice is here"
		user["select-1"] = "six"

		disabled := make(map[string]bool, 0)
		disabled["email-1"] = true
		disabled["select-1"] = true
		disabled["radio-1"] = true

		html, err := GenerateForm(c, &FormOptions{User: user, Disabled: disabled})
		So(err, ShouldBeNil)

		fmt.Printf("\n\n%v\n\n", html)

		s := strings.Replace(htmltemplate, "$form", string(html), 1)

		if err := ioutil.WriteFile("test.html", []byte(s), 0664); err != nil {
			panic(err)
		}

	})
}

const htmltemplate = `<!DOCTYPE html>
<html>
<head>
	<title>test</title>
	<style>
		form { width: 320px; margin: 50px auto; height: 100%; }
		input,select,label { width: 100%; box-sizing: border-box; }
		label { margin-bottom: 5px; display: block; }
		input,select { margin-bottom: 10px; }

		fieldset.f-radio { border: none; }
		fieldset.f-radio legend { }
	</style>
</head>
<body>

$form
</body>
</html>`
